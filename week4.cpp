#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

string reverse(string s){
	string r = s;
    int size = s.size();
    for(int i = size-1,j = 0;i>0, j<size;i--,j++){
        r[j]=s[i];
    }
    return r;
}

char maximum(string s){
    char f = s[0];
    for(int i=1;i<s.size();i++){
        f = fmax(f,s[i]);
    }
    return f;
}

char minimum(string s){
    char f = s[0];
    for(int i=1;i<s.size();i++){
        f = fmin(f,s[i]);
    }
    return f;
}


int main(){
    string s ;
    cin >> s;
    cout << reverse(s);
    cout << maximum(s) << " " << minimum(s);
    return 0;
}
