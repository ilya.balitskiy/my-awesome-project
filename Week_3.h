#include <iostream>

int fact(int n){
    if(n < 0) return 0;
    if (n == 0) return 1;
    return N * fact(N - 1); 
}

int fibonacci(int n){
    if (n == 0) return 0; 
    if (n == 1) return 1;
    return fibonacci(n-1) + fibonacci(n-2);
}

int geom(int n,int q,int a){// n - номер вычисляемого числа, a - первое число прогрессии, q - знаменатель прогрессии
    int f = q;
    for(int i = n-1;i>0;i--){
        f *= q;
    }
    return a*f;
}

int arifm(int n,int b,int a){// n - номер вычисляемого числа, a - первое число прогрессии, b - шаг прогрессии
    return a + (n-1)*b;
}
